﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour {

	public int latestLevelBeat {get; set;}

	private static Options _instance;

	public static Options Instance
	{
		get
		{
			if(_instance == null)
			{
				GameObject om = new GameObject ("OptionsController");
				om.AddComponent<Options>();
			}

			return _instance;
		}
	}

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
		_instance = this;

		InitializePrefs();
	}

	//Initializes options by loading from PlayerPref, or setting to default values if a key doesn't exist already in PlayerPref
	public void InitializePrefs()
	{
		if(PlayerPrefs.HasKey("LatestLevelBeat"))
		{
			latestLevelBeat = PlayerPrefs.GetInt("LatestLevelBeat");
			Debug.Log(latestLevelBeat);
		}
		else
		{
			latestLevelBeat = 0;
			PlayerPrefs.SetInt("LatestLevelBeat", latestLevelBeat);
		}
	}

	//Sets and saves PlayerPref settings
	public void SavePrefs()
	{
		PlayerPrefs.SetInt("LatestLevelBeat", latestLevelBeat);
	}
}