﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource musicAudioSource;
	public AudioSource soundAudioSource;
	private AudioClip musicClip;
	private AudioClip iceHitClip;

	void Awake()
	{
		if(instance == null)
			instance = this;
		else if(instance != this)
			Destroy(this.gameObject);
		
		DontDestroyOnLoad(this.gameObject);

		musicAudioSource = this.gameObject.AddComponent<AudioSource>();
		musicAudioSource.loop = true;
		if(Resources.Load("Audio/MainTheme") != null)
			musicClip = Resources.Load("Audio/MainTheme") as AudioClip;
		musicAudioSource.clip = musicClip;
		musicAudioSource.Play();

		soundAudioSource = this.gameObject.AddComponent<AudioSource>();
		if(Resources.Load("Audio/IceHit") != null)
			iceHitClip = Resources.Load("Audio/IceHit") as AudioClip;
		soundAudioSource.clip = iceHitClip;
	}
}
