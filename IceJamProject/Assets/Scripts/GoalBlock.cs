﻿using UnityEngine;
using System.Collections;

public class GoalBlock : Block {

	private float goalDist; 
	private bool isShrinking;
	public bool touchedGoal;

	void Awake()
	{
		isShrinking = false;
		touchedGoal = false;
	}

	void Update()
	{
		if(isMoving)
		{
			MoveBlock();
		}
		else if(isShrinking)
		{
			Shrink();
		}
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if(touchedGoal)
		{
			if(isMoving)
			{
				if(col.tag == "Goal")
				{
					ApproachGoal(col);
				}
			}
		}
		else
		{
			if(Vector3.Distance(col.transform.position, transform.position) <= col.transform.localScale.x - .1f)
			{
				touchedGoal = true;
			}
		}
	}
		
	protected override void MoveBlock()
	{
		if(!touchedGoal)
		{
			base.MoveBlock();
//			//This vector 3 is used to edit the block's transform position.
//			Vector3 vec3Dir = new Vector3(1*direction.x, 1*direction.y, 0);
//			
//			Vector3 moveDist = vec3Dir * speed * Time.deltaTime;
//			
//			RaycastHit2D hitInfo;
//			
//			// We raycast just bearly beyond our raycast offset distance to ensure we do not hit the object performing the raycast. 
//			hitInfo = Physics2D.Raycast(new Vector2(this.transform.position.x + (direction.x * (raycastOffset.x + .0001f)), this.transform.position.y + (direction.y * (raycastOffset.y + .0001f))), direction, moveDist.magnitude);
//			if(hitInfo.collider != null)
//			{
//				transform.position += (vec3Dir * (hitInfo.distance - .0001f));
//				isMoving = false;
//				NotificationCenter.Default.PostNotification("OnBlockStop", null);
//			}
//			else
//			{
//				transform.position += moveDist;
//			}
		}
	}

	protected void ApproachGoal(Collider2D col)
	{
		//This vector 3 is used to edit the block's transform position.
		Vector3 vec3Dir = new Vector3(1*direction.x, 1*direction.y, 0);
		
		Vector3 moveDist = vec3Dir * speed * Time.deltaTime;
				
		// We raycast just bearly beyond our raycast offset distance to ensure we do not hit the object performing the raycast. 
		if(moveDist.magnitude >= Vector3.Distance(col.transform.position, transform.position))
		{
			transform.position = col.transform.position;
			isMoving = false;
			isShrinking = true;
		}
		else
		{
			transform.position += moveDist;
		}
	}

	private void Shrink()
	{
		if(isShrinking)
		{
			if(transform.localScale.magnitude >= .05f)
			{
				transform.localScale -= Vector3.one * Time.deltaTime;
			}
			else
			{
				//				NotificationCenter.Default.PostNotification("OnGameOver", null);
				GUIControl[] thing = FindObjectsOfType(typeof(GUIControl)) as GUIControl[];
				foreach (GUIControl things in thing) {
					things.GameOverbool = true; 
				}
				SoundManager.instance.soundAudioSource.Play();
				NotificationCenter.Default.PostNotification("OnGameOver");
				isShrinking = false;
			}
		}
	}
}
