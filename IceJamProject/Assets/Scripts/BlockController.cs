﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour {

	public float swipeThreshold = 50f;

	private SceneController sC;
	private Vector2 mouseDownPos;
	private Vector2 mouseUpPos;
	private Vector2 direction;

	void Awake()
	{
		NotificationCenter.Default.AddObserver("TouchDown", OnClickDown);
		NotificationCenter.Default.AddObserver("TouchUp", OnClickUp);
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("TouchDown", OnClickDown);
		NotificationCenter.Default.RemoveObserver("TouchUp", OnClickUp);
	}

	private void OnClickDown(object o)
	{
		mouseDownPos.x = Input.mousePosition.x;
		mouseDownPos.y = Input.mousePosition.y;
	}

	private void OnClickUp(object o)
	{
		Block currentBlock = (Block)o;
		mouseUpPos.x = Input.mousePosition.x;
		mouseUpPos.y = Input.mousePosition.y;
		direction = (mouseUpPos - mouseDownPos);

		//used to determine which way to move the block
		float moveDir = Vector2.Dot(Vector2.up, direction.normalized);

		if(currentBlock.moveDir == Block.MoveDirection.Horizontal)
		{
			//If the mouse was released left of where it was pressed, and it was outside the cancel threshold, move the block left. 
			if(Input.mousePosition.x < mouseDownPos.x - swipeThreshold)
			{
//				currentBlock.ActivateBlock(Vector2.left);
				currentBlock.ActivateBlock(-Vector2.right);
			}

			//If the mouse was released right of where it was pressed, and it was outside the cancel threshold, move the block right.
			else if(Input.mousePosition.x > mouseDownPos.x + swipeThreshold)
			{
				currentBlock.ActivateBlock(Vector2.right);
			}
		}

		else if(currentBlock.moveDir == Block.MoveDirection.Vertical)
		{
			//If the mouse was released down of where it was pressed, and it was outside the cancel threshold, move the block down. 
			if(Input.mousePosition.y < mouseDownPos.y - swipeThreshold)
			{
				currentBlock.ActivateBlock(-Vector2.up);
			}

			//If the mouse was released up of where it was pressed, and it was outside the cancel threshold, move the block up.
			else if(Input.mousePosition.y > mouseDownPos.y + swipeThreshold)
			{
				currentBlock.ActivateBlock(Vector2.up);
			}
		}



		else if(currentBlock.moveDir == Block.MoveDirection.Both)
		{
			//Debug.Log(moveDir);
			//If the mouse was released left of where it was pressed, and it was outside the cancel threshold, move the block left. 
			if(moveDir >= -0.5f && moveDir <= 0.5f)
			{
				if(Input.mousePosition.x < mouseDownPos.x - swipeThreshold)
				{
					//currentBlock.ActivateBlock(Vector2.left);
					currentBlock.ActivateBlock(-Vector2.right);
				}
				
				//If the mouse was released right of where it was pressed, and it was outside the cancel threshold, move the block right.
				else if(Input.mousePosition.x > mouseDownPos.x + swipeThreshold)
				{
					currentBlock.ActivateBlock(Vector2.right);
				}	
			}

			//If the mouse was released down of where it was pressed, and it was outside the cancel threshold, move the block down. 
			if(Input.mousePosition.y < (mouseDownPos.y - swipeThreshold) && moveDir < -0.5f)
			{
				currentBlock.ActivateBlock(-Vector2.up);
			}

			//If the mouse was released up of where it was pressed, and it was outside the cancel threshold, move the block up.
			else if(Input.mousePosition.y > (mouseDownPos.y + swipeThreshold) && moveDir > 0.5f)
			{
				currentBlock.ActivateBlock(Vector2.up);
			}
		}
	}
}
