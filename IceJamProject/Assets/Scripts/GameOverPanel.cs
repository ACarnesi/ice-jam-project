﻿using UnityEngine;
using System.Collections;

public class GameOverPanel : MonoBehaviour {

	public int perfectMoveCount;
	public int currentMoveCount;


	// Use this for initialization
	void Start () {
		currentMoveCount = 0;
		NotificationCenter.Default.AddObserver("OnBlockMove", MoveCount);
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnBlockMove", MoveCount);
	}

	private void MoveCount(object o)
	{
		currentMoveCount++;
	}
}
