﻿/* What still needs to be Done
 * 
 * Win Screen script					(Done, BW)
 * 
 * Fix levels to fall in hole, JeanPaul 
 * 				make 6 new levels each!
 * 
 * Testing								
 * 
 * Make so that when a player completes a level the level is saved inside lvl select
 * 		Can wait until next due date!
 * 										(Done)
 * 
 * Add facebook sharing					(Mostly Done)
 * 
 * Fix Face book, take out of demo mode?
 * 
 * Art!									(mostly donish, if JP gives new art need to update art)
 * 						
 * 
 * Fix the levels
 * 						Can wait until the next due date
 * 
 * Fix the GUI								(Done, BW)
 * 
 * Info on how to play the game				(Done, BW)
 * 
 */

/* General Notes
 * 
 * 3 stars : perfect and above
 * 2 stars : 5 moves after perfect
 * 1 star  : anything below
 * 
 */

/* Dev. notes
 * 
 * Please Remove any debug changes (ex: if you disable an object make sure to enable it)
 * 
 * Raycasts Hit Trigger 2D must be unchecked! 
 * 			Edit -> Project Settings -> Physics2DSettings 
 */

/* QA Notes
 * 
 * Check movement
 */ 
