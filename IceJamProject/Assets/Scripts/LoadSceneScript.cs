﻿using UnityEngine;
//using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class LoadSceneScript : MonoBehaviour {

	public string name; //name of the level
	public Button buton; //stores button
	public int levelIndex;

	void Start () { // get the sprites stored in the gloabal object
		GlobalSprite[] images = FindObjectsOfType(typeof(GlobalSprite)) as GlobalSprite[];
		foreach (GlobalSprite image in images) {
			if (name == "" || levelIndex > Options.Instance.latestLevelBeat) {
				GetComponent<Image>().sprite = image.disabledPic;
			} else {
				GetComponent<Image>().sprite = image.buttonPic; 
			}

			buton = GetComponent<Button>();
			var cb= buton.colors;
			cb.pressedColor = image.pressColor;
			buton.colors = cb;
		}
	}

	public void LoadLvL () { //goes to the level stored in name
		if (name == "" || levelIndex > Options.Instance.latestLevelBeat) {
			Debug.Log("Locked");
		} else { 
			Application.LoadLevel(name); //	This does not check if the name exist! If the level does not match the string then there will be errors!
		}
	}

	public void Quit () { // quits the game
		#if UNITY_EDITOR
		{
			UnityEditor.EditorApplication.isPlaying = false;
		}
		#else
		Application.Quit();
		#endif
	}

	public void NextLvl()
	{
		int nextLevel = Application.loadedLevel + 1;
//		int nextLevel = SceneManager.GetActiveScene().buildIndex + 1;
//		if(SceneManager.GetSceneAt(nextLevel) != null)
		if(Application.CanStreamedLevelBeLoaded(nextLevel))
		{
//			SceneManager.LoadScene(nextLevel);
			Application.LoadLevel (nextLevel);
		}
		else
		{
			Debug.Log("Level desn't Exist");
		}
	}

	public void LatestLevel()
	{
		int latestLevel = Options.Instance.latestLevelBeat + 2;
//		if(SceneManager.GetSceneAt(latestLevel) != null)
		if(Application.CanStreamedLevelBeLoaded(latestLevel))
		{
//			SceneManager.LoadScene(latestLevel);
			Application.LoadLevel (latestLevel);
		}
		else
		{
			Debug.Log("Level desn't Exist");
		}
	}
}
