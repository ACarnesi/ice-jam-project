﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour 
{
	public enum MoveDirection {Horizontal, Vertical, Both};

	public MoveDirection moveDir = MoveDirection.Horizontal;
	public float speed = 3.0f;

	public bool isMoving = false;
	public Vector2 raycastOffset;
	public Vector2 direction;

	private float distTravelled;

	void Start () 
	{
		isMoving = false;

		//Because 2D raycasts return themselves, we must begin outside the object doing the raycast. This offset gives us the required distance. 
		//raycastOffset = new Vector2(transform.localScale.x / 2, transform.localScale.y / 2);
	}

	void Update()
	{
		if(isMoving)
		{
			MoveBlock();

//			GUIControl[] things = FindObjectsOfType(typeof(GUIControl)) as GUIControl[];
//			foreach (GUIControl thing in things) {
//				thing.moveCounter += 1;
//			}
		}
	}

	//Move the block in the direction of the Vector passed in until it collides with another object
	public void ActivateBlock(Vector2 d)
	{
		//This vector 3 is used to edit the block's transform position.
		Vector3 vec3Dir = new Vector3(1*d.x, 1*d.y, 0);

		Vector3 moveDist = vec3Dir * speed * Time.deltaTime;

		RaycastHit2D hitInfo;
		if(!isMoving)
		{
			hitInfo = Physics2D.Raycast(new Vector2(this.transform.position.x + (d.x * (raycastOffset.x + .0001f)), this.transform.position.y + (d.y * (raycastOffset.y + .0001f))), d, moveDist.magnitude);
			if(hitInfo.collider == null)
			{
				direction = d;
				isMoving = true;
				NotificationCenter.Default.PostNotification("OnBlockMove", null);
			}
		}
	}

	//Moves the block a certain distance based on how much time has passed, and stops the block if it is going to collide with another object. 
	protected virtual void MoveBlock()
	{
		//This vector 3 is used to edit the block's transform position.
		Vector3 vec3Dir = new Vector3(1*direction.x, 1*direction.y, 0);

		Vector3 moveDist = vec3Dir * speed * Time.deltaTime;

		RaycastHit2D hitInfo;

		// We raycast just bearly beyond our raycast offset distance to ensure we do not hit the object performing the raycast. 
		hitInfo = Physics2D.Raycast(new Vector2(this.transform.position.x + (direction.x * (raycastOffset.x + .0001f)), this.transform.position.y + (direction.y * (raycastOffset.y + .0001f))), direction, moveDist.magnitude);
		if(hitInfo.collider != null)
		{
			transform.position += (vec3Dir * (hitInfo.distance - .0001f));

			distTravelled += (vec3Dir * (hitInfo.distance - .0001f)).magnitude;
			isMoving = false;
			NotificationCenter.Default.PostNotification("OnBlockStop", null);
			SoundManager.instance.soundAudioSource.Play();
			distTravelled = 0;
		}
		else
		{
			distTravelled += moveDist.magnitude;
			transform.position += moveDist;
		}
	}
}
