﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneController : MonoBehaviour 
{
	public bool interactable = true;
	public GameObject gameOverPanel;
	public int levelNum;

	private RaycastHit2D hitInfo;
	private GameObject touchedObject;
	private int moveCount;

	void Awake()
	{
		interactable = true;
		NotificationCenter.Default.AddObserver("OnBlockStop", SetInteractable);
		NotificationCenter.Default.AddObserver("OnBlockMove", SetInteractable);
		NotificationCenter.Default.AddObserver("OnGameOver", GameOver);
		gameOverPanel.SetActive(false);
	}

	void Update()
	{
		//If running in the unity editor, use mouse inputs
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN
		if(interactable == true)
		{
			if(Input.GetMouseButtonDown(0))
			{
				hitInfo = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero, 0f);
			
				if(hitInfo.collider != null)
				{
					touchedObject = hitInfo.transform.gameObject;
			
					if(touchedObject.tag == "Block")
					{
						Block currentBlock = touchedObject.GetComponent<Block>();
						NotificationCenter.Default.PostNotification("TouchDown", currentBlock);
					}
					else return;
				}
			}
			
			if(Input.GetMouseButtonUp(0))
			{
				if(touchedObject != null)
				{
					if(touchedObject.tag == "Block")
					{
						Block currentBlock = touchedObject.GetComponent<Block>();
						NotificationCenter.Default.PostNotification("TouchUp", currentBlock);
						touchedObject = null;
					}		
				}
			}
		}
		#endif

		//Otherwise use touch input
		if(interactable)
		{
			for(int i = 0; i < Input.touchCount; i++)
			{
				Touch myTouch = Input.GetTouch(0);
				if(myTouch.phase == TouchPhase.Began)
				{
					hitInfo = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(myTouch.position).x,Camera.main.ScreenToWorldPoint(myTouch.position).y), Vector2.zero, 0f);
					
					if(hitInfo.collider != null)
					{
						touchedObject = hitInfo.transform.gameObject;
						
						if(touchedObject.tag == "MoveHoriz" || touchedObject.tag == "MoveVert")
						{
							Block currentBlock = touchedObject.GetComponent<Block>();
							NotificationCenter.Default.PostNotification("TouchDown", currentBlock);
						}
						else return;
					}
				}
				
				if(myTouch.phase == TouchPhase.Ended)
				{
					if(touchedObject != null)
					{
						if(touchedObject.tag == "MoveHoriz" || touchedObject.tag == "MoveVert")
						{
							Block currentBlock = touchedObject.GetComponent<Block>();
							NotificationCenter.Default.PostNotification("TouchUp", currentBlock);
						}
					}
				}
			}
		}
	}
		
	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnBlockStop", SetInteractable);
		NotificationCenter.Default.RemoveObserver("OnBlockMove", SetInteractable);
		NotificationCenter.Default.RemoveObserver("OnGameOver", GameOver);
	}

	public void SetInteractable(object o)
	{
		interactable = !interactable;
	}

	public void GameOver(object o)
	{	
		if(levelNum > Options.Instance.latestLevelBeat)
		{
			Options.Instance.latestLevelBeat = levelNum;
			Options.Instance.SavePrefs();
		}
	}

	public void ButtonExit()
	{
		#if UNITY_EDITOR
		{
			UnityEditor.EditorApplication.isPlaying = false;
		}
		#else
		Application.Quit();
		#endif
	}

	public void ButtonRestart()
	{
		Application.LoadLevel(Application.loadedLevelName);
	}
}
