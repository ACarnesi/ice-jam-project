﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIControl : MonoBehaviour {

	public int moveCounter; // refrenced from Blocked and BlockController

	public int BestScore; // should have the best score that the user can get

	public Text Movestext;
	public Text Finalmovestext;
	public Text GOMovesText;
	public GameObject gameoverboard; 

	public RawImage Star1;
	public RawImage Star2;
	public RawImage Star3;

	public Texture starFilled;
	public Texture starEmpty; 

	public bool GameOverbool; //refrenced by GoalBlock

	void Start () {
		NotificationCenter.Default.AddObserver("OnBlockMove", BlockMoved);
		moveCounter = 0; 
		GameOverbool = false;
	}
	
	// Update is called once per frame
	void Update () {
		Movestext.text = "Moves: " + moveCounter; 

		if (GameOverbool == true) {
			gameoverboard.active = true; 

			Finalmovestext.text = "" + moveCounter;

			#region starControl
			Star1 = Star1.GetComponent<RawImage>();
			Star2 = Star2.GetComponent<RawImage>();
			Star3 = Star3.GetComponent<RawImage>();

			if (moveCounter <= BestScore) {
				var cb= Star1.texture;
				cb = starFilled;
				Star1.texture = cb;
			} else {
				var cb= Star1.texture;
				cb = starEmpty;
				Star1.texture = cb;
			}

			if (moveCounter <= (BestScore + 5)) {
				var cb= Star2.texture;
				cb = starFilled;
				Star2.texture = cb;
			} else {
				var cb= Star2.texture;
				cb = starEmpty;
				Star2.texture = cb;
			}

			if (moveCounter <= (BestScore + 10)) {
				var cb= Star3.texture;
				cb = starFilled;
				Star3.texture = cb;
			} else {
				var cb= Star3.texture;
				cb = starEmpty;
				Star3.texture = cb;
			} 
			#endregion

		} else {
			gameoverboard.active = false; 
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnBlockStop", BlockMoved);
	}

	private void BlockMoved(object o)
	{
			moveCounter++;
	}
}
